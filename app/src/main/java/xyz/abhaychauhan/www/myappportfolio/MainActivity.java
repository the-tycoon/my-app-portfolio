package xyz.abhaychauhan.www.myappportfolio;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button btnPopularMovies;
    Button btnStock;
    Button btnBuildBigger;
    Button btnMaterial;
    Button btnUbiquitous;
    Button btnCapstone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Initialising buttons
        btnPopularMovies = (Button) findViewById(R.id.btn_popular_movies);
        btnStock = (Button) findViewById(R.id.btn_stock_hawk);
        btnBuildBigger = (Button) findViewById(R.id.btn_build_bigger);
        btnMaterial = (Button) findViewById(R.id.btn_make_material);
        btnUbiquitous = (Button) findViewById(R.id.btn_go_ubiquitous);
        btnCapstone = (Button) findViewById(R.id.btn_capstone);

        //Setting SetOnClickListener
        btnPopularMovies.setOnClickListener(this);
        btnStock.setOnClickListener(this);
        btnBuildBigger.setOnClickListener(this);
        btnMaterial.setOnClickListener(this);
        btnUbiquitous.setOnClickListener(this);
        btnCapstone.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        if (view == findViewById(R.id.btn_popular_movies)) {
            displayMessage("Popular Movies");
        } else if (view == findViewById(R.id.btn_stock_hawk)) {
            displayMessage("Stock Hawk");
        } else if (view == findViewById(R.id.btn_build_bigger)) {
            displayMessage("Build it Bigger");
        } else if (view == findViewById(R.id.btn_make_material)) {
            displayMessage("Make Your App Material");
        } else if (view == findViewById(R.id.btn_go_ubiquitous)) {
            displayMessage("Go Ubiquitous");
        } else {
            displayMessage("Capstone");
        }
    }

    /**
     * Function will display toast message
     *
     * @param string
     * @return
     */
    private void displayMessage(String string) {
        Toast.makeText(this, "This will launch my " + string + " app!", Toast.LENGTH_SHORT).show();
    }
}
